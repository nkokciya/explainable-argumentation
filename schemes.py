def string_parser(pred_dict,s):
    import re
    
    def parse_for_key(key, reg, st):
	    return re.findall(key+reg, st)

    def reg_builder(key):
        r1 = "\(([A-z0-9_]+)\)"
        r2 = "\(([,A-z0-9_]+)\)"
      
        if len(pred_dict[key]) == 1:
            return r1
        else:
            return r2

    # a dictionary to keep the result
    res = {}
    bindings = {}
    for p in pred_dict.keys():
        res[p] = parse_for_key(p, reg_builder(p), s)[0].split(',')
      
        i = 0
        while i < len(res[p]):
            var = pred_dict[p][i]
            if var not in bindings.keys():
                bindings[var] = res[p][i]
            i+=1
    
    return bindings
    
"""
    s: an argument string to parse. 
    example input: s = "aspt([goal(rbp),promotes(ccb,rbp),indicatedAt(ccb,s1)],action(ccb)) "
"""    
def aspt(s):
    pred_dict= {"goal":["G"], "action":["A"], "indicatedAt":["A","S"],"promotes":["A","G"]}
    
    bindings = string_parser(pred_dict,s)
    
    explanation = "Treatment '{}' should be considered at step '{}' as it promotes the goal '{}'.".format(bindings["A"],bindings["S"],bindings["G"])
    
    return bindings,explanation
    
"""
    s: an argument string to parse. 
    example input: s = "sef([greater(t2,t1),effective(ccb,t1)],action(ccb))"
"""    
def sef(s):
    pred_dict= {"effective":["A","T"], "action":["A"]}
    
    bindings = string_parser(pred_dict,s)
    
    explanation = "Treatment '{}' can be considered as it was an effective treatment at time '{}'.".format(bindings["A"],bindings["T"])
    
    return bindings,explanation
    
"""
    s: an argument string to parse. 
    example input: s = "se([greater(t2,t1),offered(ccb,t1),may_cause(ccb,swollen_ankles)],notaction(ccb))"
"""    
def se(s):
    pred_dict= {"offered":["A","T"], "may_cause":["A","S"]}
    
    bindings = string_parser(pred_dict,s)
    
    explanation = "Treatment '{}' may cause a side effect such as '{}'. Hence, you might consider another treatment instead of '{}'.".format(bindings["A"],bindings["S"],bindings["A"])
    
    return bindings,explanation          

"""
    s: an argument string to parse. 
    example input: s = "alt([aspt([goal(rbp),promotes(thiazide,rbp),indicatedAt(thiazide,s1)],action(thiazide)),aspt([goal(rbp),promotes(ccb,rbp),indicatedAt(ccb,s1)],action(ccb)),alternative(thiazide,ccb),indicatedAt(thiazide,s1),indicatedAt(ccb,s1)],attacks(aspt([goal(rbp),promotes(thiazide,rbp),indicatedAt(thiazide,s1)],action(thiazide)),aspt([goal(rbp),promotes(ccb,rbp),indicatedAt(ccb,s1)],action(ccb))))"
"""    
def alt(s):
    pred_dict= {"goal":["G"],"alternative":["X","Y"], "promotes":["X","G"], "promotes":["Y","G"], "indicatedAt":["X","S"]}
    
    bindings = string_parser(pred_dict,s)
    
    explanation = "Treatment '{}' and treatment '{}' promote the same goal '{}' at step '{}', and '{}' is an alternative to '{}'; hence, they should not be offered together.".format(bindings["X"],bindings["Y"],bindings["G"],bindings["S"],bindings["X"],bindings["Y"])
    
    return bindings,explanation        

"""
    s: an argument string to parse. 
    example input: s = "tcq([sef([greater(t2,t1),effective(ccb,t1)],action(ccb)),se([greater(t2,t1),offered(ccb,t1),may_cause(ccb,swollen_ankles)],notaction(ccb)),challenges(sef,se)],attacks(sef([greater(t2,t1),effective(ccb,t1)],action(ccb)),se([greater(t2,t1),offered(ccb,t1),may_cause(ccb,swollen_ankles)],notaction(ccb))))"
"""    
def tcq(s):
    pred_dict= {"challenges":["S1","S2"]}
    
    bindings = string_parser(pred_dict,s)
    
    explanation = "The scheme '{}' is a critical question of the scheme '{}'".format(bindings["S1"],bindings["S2"])
    
    return bindings,explanation