# explainable-argumentation

This Python implementation shows the applicability of our approach to a medical scenario, Baula, as described in the paper. 

Our paper called "An argumentation-based approach for generating domain-specific explanations" has been accepted to EUMAS2020. We will share our 
paper once it is published. The authors of this paper are Nadin Kokciyan, Simon Parsons, Isabel Sassoon, Elizabeth Sklar and Sanjay Modgil. 

*  [DLV](http://www.dlvsystem.com/dlv/) should be installed on the system in order to run the code. 
*  In the root directory, the following code should be invoked on the terminal:
`python evalaf_expaf.py baula/kb.dl baula/attacks.dl baula/baula.dl`
* After running the code, the terminal will display acceptable arguments and attacks together with explanations. 

* 'outputs' folder already contains the outputs that you should get after running code. Note that the generation of graphs is optional, 
they are visual representations of the constructed argumentation frameworks.